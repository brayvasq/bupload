$(function(){
   if(localStorage.getItem("token")){

       /**
        * Añadiendo listener a botón submit del formulario de subida de archivo
        * @type {Element}
        */
       var singleUploadForm = document.querySelector('#singleUploadForm');
       var singleFileUploadInput = document.querySelector('#singleFileUploadInput');
       singleUploadForm.addEventListener('submit', function(event){
           var files = singleFileUploadInput.files;
           uploadSingleFile(files[0]);
           event.preventDefault();
       }, true);

       /**
       * SECCIÓN DONDE SE LISTA LOS ARCHIVOS
       * */
       setInterval(function(){
           var data = new FormData();
           data.append("file", "");

           var xhr = new XMLHttpRequest();
           xhr.withCredentials = true;

           xhr.addEventListener("readystatechange", function () {
               if (this.readyState === 4) {
                   var data = JSON.parse(this.responseText)
                   console.log(data);
                   var html_table = ""
                   $("#table-files").html("");
                   for (var i = 0; i < data.length; i++) {
                       html_table += getTemplate(data[i]);
                   }
                   $("#table-files").html(html_table);
               }
           });

           xhr.open("GET", "http://localhost:8080/api/files/user");
           xhr.setRequestHeader("authorization", "Bearer "+localStorage.getItem("token"));
           xhr.setRequestHeader("cache-control", "no-cache");
           xhr.setRequestHeader("postman-token", "dab8ee28-9da3-94ac-2951-3fc2c0525f3f");

           xhr.send(data);

       },2000)

   }else{
       window.location = "index.html"
   }
});

function uploadSingleFile(file){
    var data = new FormData();
    data.append("file", file);

    var xhr = new XMLHttpRequest();
    xhr.withCredentials = true;

    xhr.addEventListener("readystatechange", function () {
        if (this.readyState === 4) {
            console.log(this.responseText);
        }
    });

    xhr.open("POST", "http://localhost:8080/api/files/upload");
    xhr.setRequestHeader("authorization", "Bearer "+localStorage.getItem("token"));
    xhr.setRequestHeader("cache-control", "no-cache");
    xhr.setRequestHeader("postman-token", "dcdfe152-c8e3-43e1-c19c-a93eba219b62");

    xhr.send(data);
}

function getTemplate(item){
    console.log(item);
    return "<tr> <td>"+item.fileName+"</td>\n" +
        "                    <td>"+item.fileType+"</td>\n" +
        "                    <td>"+item.size+"</td>\n" +
        "                    <td><a href='http://localhost:8080/api/files/downloadFile/"+item.fileName+"'>Download</a></td>\n" +
        "                    <td><input type='button' class='btn btn-danger' onclick='deleteItem(\""+item.fileName+"\")' value='Delete'></td></tr>";
}

function deleteItem(item){
    console.log(item);
    var data = null;

    var xhr = new XMLHttpRequest();
    xhr.withCredentials = true;

    xhr.addEventListener("readystatechange", function () {
        if (this.readyState === 4) {
            console.log(this.responseText);
        }
    });

    xhr.open("DELETE", "http://localhost:8080/api/files/deleteFile/"+item);
    xhr.setRequestHeader("cache-control", "no-cache");
    xhr.setRequestHeader("postman-token", "9e685431-5c7c-3727-2152-86ee8c37fb72");

    xhr.send(data);
}