package com.example.uploadspring.repository;

import com.example.uploadspring.model.Role;
import com.example.uploadspring.model.RoleName;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Interfaz que interactua con la base de datos y con el modelo Role mediante JPA
 *
 * @Repository indica que es un repositorio para la aplicaión spring
 */
@Repository
public interface RoleRepository extends JpaRepository<Role,Long> {
    Optional<Role> findByName(RoleName roleName);
}
