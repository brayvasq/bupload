package com.example.uploadspring.repository;

import com.example.uploadspring.model.DBFile;
import com.example.uploadspring.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Interfaz que interactua con la base de datos y con el modelo DBFile mediante JPA
 *
 * @Repository indica que es un repositorio para la aplicaión spring
 */
@Repository
public interface DBFileRepository extends JpaRepository<DBFile, Integer> {
    List<DBFile> findByUser(User user);
    Optional<?> removeAllByFileName(String fileName);
    Optional<DBFile> findByFileName(String fileName);
}