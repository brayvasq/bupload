package com.example.uploadspring.controller;

import com.example.uploadspring.model.DBFile;
import com.example.uploadspring.payload.ApiResponse;
import com.example.uploadspring.payload.UploadFileResponse;
import com.example.uploadspring.service.DBFileStorageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Clase que maneja las peticiones para los archivos
 *
 * @RestController Indica que el controlador es REST, esto indica que las respuestas van a
 *                Escribirse en el cuerpode la respuesta y no en un template renderizado
 * @RequestMapping Indica la url que va a manejar el controlador
 */
@RestController
@RequestMapping("/api/files")
public class FileController {
    private static final Logger logger = LoggerFactory.getLogger(FileController.class);
    private String url = "http://localhost:8080/api/files/";

    @Autowired
    private DBFileStorageService fileStorageService;

    /**
     * Método que obtiene los datos del fichero a cargar y el usuario autenticado
     *
     * @param file
     *
     * @return
     */
    @PostMapping("/upload")
    public UploadFileResponse uploadFile(@RequestParam("file") MultipartFile file){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String currentPrincipalName = authentication.getName();
        DBFile dbFile = fileStorageService.storeFile(file, currentPrincipalName);

        return new UploadFileResponse(dbFile.getFileName(), dbFile.getUrlFile(),
                dbFile.getFileType(), dbFile.getFileSize());
    }

    /**
     * Método que obtiene los ficheros del usuario autenticado
     * y mapea la respuesta con el payload definido
     *
     * @return
     */
    @GetMapping("/user")
    public List<UploadFileResponse> getByUser(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String currentPrincipalName = authentication.getName();

        List<DBFile> files = fileStorageService.getAllByUser(currentPrincipalName);
        return files.stream().map(file -> new UploadFileResponse(file.getFileName(), this.url+file.getUrlFile(),
                file.getFileType(), file.getFileSize())).collect(Collectors.toList());
    }

    /**
     * Obtiene los archivos enviados en la petición y llama el método
     * uploadFile para cada archivo a subir
     *
     * @param files
     *
     * @return
     */
    @PostMapping("/uploadMultipleFiles")
    public List<UploadFileResponse> uploadMultipleFiles(@RequestParam("files") MultipartFile[] files) {
        return Arrays.asList(files)
                .stream()
                .map(file -> uploadFile(file))
                .collect(Collectors.toList());
    }

    /**
     * Método que obtiene la ruta del fichero y lo devuelve para su descarga
     *
     * @param fileName
     * @param request
     *
     * @return
     */
    @GetMapping("/downloadFile/{fileName:.+}")
    public ResponseEntity<Resource> downloadFile(@PathVariable String fileName, HttpServletRequest request) {
        // Cargando el fichero como un recurso
        Resource resource = fileStorageService.loadFileAsResource(fileName);

        // Obteniendo el tipo de archivo
        String contentType = null;
        try {
            contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
        } catch (IOException ex) {
            logger.info("Could not determine file type.");
        }

        // Asignando un tipo por defecto
        if(contentType == null) {
            contentType = "application/octet-stream";
        }

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(contentType))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                .body(resource);
    }

    /**
     * Método que obtiene el archivo a traves de su nombre y lo elimina
     * de la base de datos y de la carpeta uploads
     *
     * @param fileName
     * @param request
     *
     * @return
     */
    @DeleteMapping("/deleteFile/{fileName:.+}")
    public ApiResponse deleteFile(@PathVariable String fileName, HttpServletRequest request) {
        // Cargando el archivo como un recurso
        Resource resource = fileStorageService.loadFileAsResource(fileName);

        try {
            resource.getFile().delete();
        } catch (IOException e) {
            e.printStackTrace();
        }
        fileStorageService.deleteFile(fileName);
        return new ApiResponse(true, fileName+" ha sido eliminado!");
    }

    /**
     * Obtiene los datos de un archivo específico con su nombre y
     * lo devuelve en el formato del payload definido
     *
     * @param fileName
     *
     * @return
     */
    @GetMapping("/file/{fileName:.+}")
    public UploadFileResponse getFile(@PathVariable String fileName) {
        // Cargando los datos del archivo desde la base de datos
        DBFile dbFile = fileStorageService.getFile(fileName);

        return new UploadFileResponse(dbFile.getFileName(), this.url+dbFile.getUrlFile(),
                dbFile.getFileType(), dbFile.getFileSize());
    }

    /**
     * Obtiene todos los archivos existentes y los mapea
     * con el payload definido
     *
     * @return
     */
    @GetMapping("/all")
    public List<UploadFileResponse> getFiles() {
        List<DBFile> files = fileStorageService.getAll();
        return files.stream().map(file -> new UploadFileResponse(file.getFileName(), this.url+file.getUrlFile(),
                file.getFileType(), file.getFileSize())).collect(Collectors.toList());
    }
}
