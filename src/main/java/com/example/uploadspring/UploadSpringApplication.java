package com.example.uploadspring;

import com.example.uploadspring.property.FileStorageProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters;

import javax.annotation.PostConstruct;
import java.util.TimeZone;

@SpringBootApplication
// Incluyendo clases que poseen items de la configuración del archivo .properties
@EnableConfigurationProperties({
		FileStorageProperties.class
})
@EntityScan(basePackageClasses = {
        UploadSpringApplication.class,
        Jsr310JpaConverters.class
})
public class UploadSpringApplication {

    @PostConstruct
    void init() {
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
    }

	public static void main(String[] args) {
		SpringApplication.run(UploadSpringApplication.class, args);
	}

}

