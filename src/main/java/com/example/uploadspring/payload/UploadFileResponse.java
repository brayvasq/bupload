package com.example.uploadspring.payload;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Clase que funciona como serializer o encapsulador para devolver los
 * datos relevantes de un registro de fichero
 *
 * @Data Resume los Getter,Setters y otros elementos a través de Lombok
 * @AllArgsConstructor Crea un constructor a través de Lombok con todos los atributos de la clase como argumentos
 * */
@Data
@AllArgsConstructor
public class UploadFileResponse {
    private String fileName;
    private String fileDownloadUri;
    private String fileType;
    private long size;
}
