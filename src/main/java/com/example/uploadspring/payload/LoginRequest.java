package com.example.uploadspring.payload;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * Clase que contiene los campos que deben ser recibidos
 * en la petición login.
 *
 * @Data Resume los Getter,Setters y otros elementos a través de Lombok
 *
 * Constraints:
 *      @NotBlank Verifica que el campo no este vacío
 */
@Data
public class LoginRequest {
    @NotBlank
    private String usernameOrEmail;

    @NotBlank
    private String password;
}
