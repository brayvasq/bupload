package com.example.uploadspring.payload;

import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

/**
 * Clase que contiene los datos que deben ser recibidos en
 * la peticion de registro de usuario
 *
 * @Data Resume los Getter,Setters y otros elementos a través de Lombok
 * @AllArgsConstructor Crea un constructor a través de Lombok con todos los atributos de la clase como argumentos
 *
 * Constraints:
 *      @NotBlank Verifica que el campo no este vacío
 *      @Size Verifica la longitud del campo en un rango dado.
 *      @Email Valida que el campo concuerde con el formato de correo.
 * */
@Data
public class SignUpRequest {
    @NotBlank
    @Size(min = 4, max = 40)
    private String name;

    @NotBlank
    @Size(min = 3, max = 15)
    private String username;

    @NotBlank
    @Size(max = 40)
    @Email
    private String email;

    @NotBlank
    @Size(min = 6, max = 20)
    private String password;
}
