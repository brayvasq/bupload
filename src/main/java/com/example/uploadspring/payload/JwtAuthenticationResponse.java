package com.example.uploadspring.payload;

import lombok.Data;

/**
 * Clase que contiene la estructura de respuesta del token JWT
 * para la petición login
 *
 * @Data Resume los Getter,Setters y otros elementos a través de Lombok
 *
 * Constraint:
 *      @NotBlank Verifica que el campo no este vacío
 */
@Data
public class JwtAuthenticationResponse {
    private String accessToken;
    private String tokenType = "Bearer";

    public JwtAuthenticationResponse(String accessToken) {
        this.accessToken = accessToken;
    }
}
