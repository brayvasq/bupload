package com.example.uploadspring.payload;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Clase que funciona como serializador json para devolver respuestas simples para
 * peticiones en las APIs
 *
 * @Data Resume los Getter,Setters y otros elementos a través de Lombok
 * @AllArgsConstructor Crea un constructor a través de Lombok con todos los atributos de la clase como argumentos
 *
 * */
@Data
@AllArgsConstructor
public class ApiResponse {
    private Boolean success;
    private String message;
}
