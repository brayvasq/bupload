package com.example.uploadspring.property;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Clase que extrae las propiedades definidas en el archivo .properties para los ficheros
 *
 * @Data Resume los Getter,Setters y otros elementos a través de Lombok
 * @ConfigurationProperties indica las propiedades que va a extraer
 */
@ConfigurationProperties(prefix = "file")
@Data
public class FileStorageProperties {
    private String uploadDir;
}
