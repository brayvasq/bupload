package com.example.uploadspring.service;

import com.example.uploadspring.exception.FileStorageException;
import com.example.uploadspring.exception.MyFileNotFoundException;
import com.example.uploadspring.model.DBFile;
import com.example.uploadspring.model.User;
import com.example.uploadspring.property.FileStorageProperties;
import com.example.uploadspring.repository.DBFileRepository;
import com.example.uploadspring.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.List;

/**
 * Clase que actua como intermedio entre el controlador y los repositorios, además
 * contiene la logica principal
 *
 * @Service Indica que la clase creada es un servicio
 * @Transactional Indica que el método va a realizar una transacción sobre la base de datos
 */
@Service
public class DBFileStorageService {

    private final Path fileStorageLocation;

    @Autowired
    private DBFileRepository dbFileRepository;

    @Autowired
    private UserRepository userRepository;

    /**
     * Método constructor que inyecta la dependecia FileStorageProperties, que contiene
     * la propiedades de configuración
     *
     * @param fileStorageProperties
     */
    @Autowired
    public DBFileStorageService(FileStorageProperties fileStorageProperties) {
        this.fileStorageLocation = Paths.get(fileStorageProperties.getUploadDir())
                .toAbsolutePath().normalize();

        try {
            Files.createDirectories(this.fileStorageLocation);
        } catch (Exception ex) {
            throw new FileStorageException("Could not create the directory where the uploaded files will be stored.", ex);
        }
    }

    /**
     * Método que guarda el fichero en la carpeta uploads y los datos en la base de datos
     *
     * @param file archivo a guardar en el directorio uploads
     * @param username nombre del usuario logeado
     *
     * @return
     */
    public DBFile storeFile(MultipartFile file,String username) {
        // Normalizando la ruta
        String fileName = StringUtils.cleanPath(file.getOriginalFilename());

        try {
            // Verificando si la ruta contiene caracteres invalidos
            if(fileName.contains("..")) {
                throw new FileStorageException("Sorry! Filename contains invalid path sequence " + fileName);
            }

            // Copia el fichero a la ruta, y lo reemplaza si existe
            Path targetLocation = this.fileStorageLocation.resolve(fileName);
            Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);
            User user = userRepository.findByUsernameOrEmail(username, username)
                    .orElseThrow(() ->
                            new UsernameNotFoundException("User not found with username or email : " + username)
                    );

            DBFile dbFile = new DBFile(fileName,file.getContentType(),file.getSize(),"downloadFile/"+fileName);
            dbFile.setUser(user);

            return dbFileRepository.save(dbFile);
        } catch (IOException ex) {
            throw new FileStorageException("Could not store file " + fileName + ". Please try again!", ex);
        }
    }

    /**
     * Obtiene la información de un archivo mediante su nombre
     *
     * @param fileName nombre del archivo del que se extraerá la información
     *
     * @return
     */
    public DBFile getFile(String fileName) {
        return dbFileRepository.findByFileName(fileName)
                .orElseThrow(() -> new MyFileNotFoundException("DBFile not found with id " + fileName));
    }

    /**
     * Obtiene los datos de todos los archivos
     *
     * @return
     */
    public List<DBFile> getAll(){
        return dbFileRepository.findAll();
    }

    /**
     * Obtiene los datos de todos los archivos de un usuario
     *
     * @param name nombre del usuario logueado
     *
     * @return
     */
    public List<DBFile> getAllByUser(String name){
        User user = userRepository.findByUsernameOrEmail(name, name)
                .orElseThrow(() ->
                        new UsernameNotFoundException("User not found with username or email : " + name)
                );
        return dbFileRepository.findByUser(user);
    }

    /**
     * Método que borra el registro de un archivo de la base de datos
     *
     * @param fileName nombre del archivo a eliminar
     */
    @Transactional
    public void deleteFile(String fileName){
        dbFileRepository.removeAllByFileName(fileName);
    }

    /**
     * Método que obtiene un archivo como recurso
     *
     * @param fileName nombre del archivo a obtener
     *
     * @return
     */
    public Resource loadFileAsResource(String fileName) {
        try {
            Path filePath = this.fileStorageLocation.resolve(fileName).normalize();
            Resource resource = new UrlResource(filePath.toUri());
            if(resource.exists()) {
                return resource;
            } else {
                throw new MyFileNotFoundException("DBFile not found " + fileName);
            }
        } catch (MalformedURLException ex) {
            throw new MyFileNotFoundException("DBFile not found " + fileName, ex);
        }
    }

}
