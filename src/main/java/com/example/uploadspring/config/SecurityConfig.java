package com.example.uploadspring.config;

import com.example.uploadspring.security.CustomUserDetailsService;
import com.example.uploadspring.security.JwtAuthenticationEntryPoint;
import com.example.uploadspring.security.JwtAuthenticationFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.BeanIds;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

/**
 * Clase que contiene la configuración para spring security
 *
 * @Configuration Indica que es una clase de configuración.
 * @EnableWebSecurity Indica que es una clase de configuración
 * @EnableGlobalMethodSecurity Habilita las anotaciones de seguridad
 *      securedEnabled: Habilita la anotación @Secured
 *      jsr250Enabled: Habilita la anotación @RolesAllowed
 *      prePostEnabled: Habilita las anotaciones @PreAuthorize y @PostAuthorize
 */
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(
        securedEnabled = true,
        jsr250Enabled = true,
        prePostEnabled = true
)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    // Permite autenticar un usuario
    @Autowired
    CustomUserDetailsService customUserDetailsService;

    // Retorna un error 401 para recursos protegidos
    @Autowired
    private JwtAuthenticationEntryPoint unauthorizedHandler;

    // Lee, valida y carga el token JWT
    @Bean
    public JwtAuthenticationFilter jwtAuthenticationFilter() {
        return new JwtAuthenticationFilter();
    }

    /**
     * Permite definir cómo se manejará la autenticación.
     * Se puede usar autenticación en memoria, JDBC, autenticación perzonalizada, etc.
     *
     * @param authenticationManagerBuilder
     *
     * @throws Exception
     */
    @Override
    public void configure(AuthenticationManagerBuilder authenticationManagerBuilder) throws Exception {
        // Se define la clase que recupera los usuarios y el algoritmo para procesar las contraseñas
        authenticationManagerBuilder
                .userDetailsService(customUserDetailsService)
                .passwordEncoder(passwordEncoder());
    }

    /**
     * @return
     *
     * @throws Exception
     */
    @Bean(BeanIds.AUTHENTICATION_MANAGER)
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    /**
     * Método que devuelve el algoritmo que se utilizará para procesar las contraseñas
     *
     * @return BCryptPasswordEncoder Objeto que procesara las contraseñas
     */
    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    /**
     * Permite configurar aspectos como cors, csrf, manejo de sesiones y
     * adicionar reglas para proteger los recursos de la aplicación.
     *
     * Se desactiva el uso de cookies
     * Se activa la configuración CORS
     * Se desactiva el filtro CSRF
     * Se configura las rutas que necesitan autorización
     * Se configura las rutas publicas
     * Se configura el acceso a archivos estáticos
     * Se indica el filtro de seguridad a usar
     *
     * @param http
     *
     * @throws Exception
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .cors()
                .and()
                .csrf()
                .disable()
                .exceptionHandling()
                .authenticationEntryPoint(unauthorizedHandler)
                .and()
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .authorizeRequests()
                .antMatchers("/",
                        "/favicon.ico",
                        "/**/*.png",
                        "/**/*.gif",
                        "/**/*.svg",
                        "/**/*.jpg",
                        "/**/*.html",
                        "/**/*.css",
                        "/**/*.js")
                .permitAll()
                .antMatchers("/api/auth/**")
                .permitAll()
                .antMatchers("/api/user/checkUsernameAvailability", "/api/user/checkEmailAvailability")
                .permitAll()
                .antMatchers("/api/files/**", "/api/users/**")
                .permitAll()
                .anyRequest()
                .authenticated();

        // Add our custom JWT security filter
        http.addFilterBefore(jwtAuthenticationFilter(), UsernamePasswordAuthenticationFilter.class);

    }

}
