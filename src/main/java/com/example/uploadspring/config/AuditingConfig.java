package com.example.uploadspring.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

/**
 * Archivo de configuración de las clases de auditoria
 *
 * @Configuration Indica que es una clase de configuración.
 * @EnableJpaAuditing Habilita las clases de auditoría
 */

@Configuration
@EnableJpaAuditing
public class AuditingConfig {
}
