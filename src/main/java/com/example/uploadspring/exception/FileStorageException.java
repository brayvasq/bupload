package com.example.uploadspring.exception;

/**
 * Clase para definir los mensajes de error para los archivos
 */
public class FileStorageException extends RuntimeException{
    public FileStorageException(String message) {
        super(message);
    }

    public FileStorageException(String message, Throwable cause) {
        super(message, cause);
    }
}
