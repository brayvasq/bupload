package com.example.uploadspring.security;

import org.springframework.security.core.annotation.AuthenticationPrincipal;

import java.lang.annotation.*;

/**

 * */

/**
 * Interfaz que funciona como una anotación personalizada
 * Esto nos permite reducir todas la anotaciones indicadas a una sola
 *
 * Meta-anotaciones:
 *      Indican que se agruparán varias anotaciones en una.
 *      @Target
 *      @Retention
 *      @Documented
 *
 * @AuthenticationPrincipal esta anotación permite acceder al usuario que está autenticado
 *
 */
@Target({ElementType.PARAMETER, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@AuthenticationPrincipal
public @interface CurrentUser {

}
