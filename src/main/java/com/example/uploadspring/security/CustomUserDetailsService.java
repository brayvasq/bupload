package com.example.uploadspring.security;

import com.example.uploadspring.model.User;
import com.example.uploadspring.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

/**
 * Servicio que va a cargar los datos del usuario autenticado
 * Esta clase extiende {@link UserDetailsService} que define el método loadUserByUsername
 *
 * @Service Indica que la clase creada es un servicio
 * @Transactional Indica que el método va a realizar una transacción sobre la base de datos
 */
@Service
public class CustomUserDetailsService implements UserDetailsService {
    @Autowired
    UserRepository userRepository;

    /**
     * Método usado por Spring Security
     *
     * @param usernameOrEmail username o email del usuario para identificar si existe
     *
     * @return
     *
     * @throws UsernameNotFoundException
     */
    @Override
    @Transactional
    public UserDetails loadUserByUsername(String usernameOrEmail)
            throws UsernameNotFoundException {
        // Deja que la autenticación sea por usuario o email
        User user = userRepository.findByUsernameOrEmail(usernameOrEmail, usernameOrEmail)
                .orElseThrow(() ->
                        new UsernameNotFoundException("User not found with username or email : " + usernameOrEmail)
                );

        return UserPrincipal.create(user);
    }

    /**
     * Método usado por JWTAuthenticationFilter
     *
     * @param id identificador del usuario
     *
     * @return
     */
    @Transactional
    public UserDetails loadUserById(Long id) {
        User user = userRepository.findById(id).orElseThrow(
                () -> new UsernameNotFoundException("User not found with id : " + id)
        );

        return UserPrincipal.create(user);
    }

}
