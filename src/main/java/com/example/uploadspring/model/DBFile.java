package com.example.uploadspring.model;

import com.example.uploadspring.model.audit.DateAudit;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

/**
 * Clase que define el modelo de archivo para ser guardado en la base de datos
 *
 * @Entity Indica que la clase será mapeada en la base de datos.
 * @Table Indica la tabla que se asociará a la entidad
 * @Data Resume los Getter,Setters y otros elementos a través de Lombok
 * @NoArgsConstructor Crea un constructor vacío a través de Lombok
 * @AllArgsConstructor Crea un constructor a través de Lombok con todos los atributos de la clase como argumentos
 *
 * Constraints:
 *      @Id Indica que el campo es una primary key
 *      @GeneratedValue Indica que el campo va a ser autogenerado
 *      @ManyToOne Indica una relación de muchos a uno, en este caso Muchos archivos(DBFile) pertenecen a un usuario(User)
 *      @JoinColumn Indica el campo de relación
 */
@Entity
@Table(name = "files")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class DBFile extends DateAudit {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    private String fileName;
    private String fileType;
    private long fileSize;
    private String urlFile;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    /**
     * Constructor personalizado
     * @param fileName nombre del archivo
     * @param fileType tipo de archivo
     * @param fileSize tamaño del archivo
     * @param urlFile url de descarga del archivo
     */
    public DBFile(String fileName, String fileType, long fileSize, String urlFile) {
        this.fileName = fileName;
        this.fileType = fileType;
        this.fileSize = fileSize;
        this.urlFile = urlFile;
    }
}
