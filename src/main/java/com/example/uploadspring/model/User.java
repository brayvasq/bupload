package com.example.uploadspring.model;

import com.example.uploadspring.model.audit.DateAudit;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.NaturalId;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Set;

/**
 * Clase que define el modelo de usuario para ser guardado en la base de datos
 *
 * @Entity Indica que la clase será mapeada en la base de datos.
 * @Table Indica la tabla que se asociará a la entidad
 * @UniqueConstraint Indica que un campo debe ser unico
 * @Data Resume los Getter,Setters y otros elementos a través de Lombok
 * @NoArgsConstructor Crea un constructor vacío a través de Lombok
 *
 * Contraints:
 *      @Id Indica que el campo es una primary key
 *      @GeneratedValue Indica que el campo va a ser autogenerado
 *      @NotBlank Verifica que el campo no este vacío
 *      @Size Verifica la longitud del campo en un rango dado.
 *      @Email Valida que el campo concuerde con el formato de correo.
 */
@Entity
@Table(name="users", uniqueConstraints = {
        @UniqueConstraint(columnNames = {
                "username"
        }),
        @UniqueConstraint(columnNames = {
                "email"
        })
})
@Data
@NoArgsConstructor
public class User extends DateAudit {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    @Size(max = 40)
    private String name;

    @NotBlank
    @Size(max = 15)
    private String username;

    @NaturalId
    @NotBlank
    @Size(max = 40)
    @Email
    private String email;

    @NotBlank
    @Size(max = 100)
    private String password;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "user_roles",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id"))
    private Set<Role> roles = new HashSet<>();

    public User(String name, String username, String email, String password) {
        this.name = name;
        this.username = username;
        this.email = email;
        this.password = password;
    }
}
