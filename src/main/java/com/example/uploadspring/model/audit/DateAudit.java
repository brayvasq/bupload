package com.example.uploadspring.model.audit;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;
import java.time.Instant;

/**
 * Clase que audita las fechas de los modelos
 *
 * @EntityListeners indica el listener que asignará los valores
 * @JsonIgnoreProperties Indica los valores que se ignorarán en las respuestas JSON
 * @Data Resume los Getter,Setters y otros elementos a través de Lombok
 *
 * Constraints:
 *      @CreatedDate Indica que el campo contiene la fecha en que se crea un registro
 *      @LastModifiedDate Indica que el campo contiene la fecha en que se modificó un registro
 *      @Column Indica propiedades directas de un campo
 */
@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(
        value = {"createdAt", "updatedAt"},
        allowGetters = true
)
@Data
public abstract class DateAudit {
    @CreatedDate
    @Column(nullable = false, updatable = false)
    private Instant createdAt;

    @LastModifiedDate
    @Column(nullable = false)
    private Instant updatedAt;
}
