package com.example.uploadspring.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.NaturalId;

import javax.persistence.*;

/**
 * Clase que define el modelo de rol para ser guardado en la base de datos
 *
 * @Entity Indica que la clase será mapeada en la base de datos.
 * @Table Indica la tabla que se asociará a la entidad
 * @Data Resume los Getter,Setters y otros elementos a través de Lombok
 * @NoArgsConstructor Crea un constructor vacío a través de Lombok
 *
 * Constraints:
 *      @Id Indica que el campo es una primary key
 *      @GeneratedValue Indica que el campo va a ser autogenerado
 *      @Enumerated mapea los valores de una enumeración en un campo
 *      @Column Indica propiedades directas de un campo
 */
@Entity
@Table(name = "roles")
@Data
@NoArgsConstructor
public class Role {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(EnumType.STRING)
    @NaturalId
    @Column(length = 60)
    private RoleName name;

    /**
     * Constructor personalizado
     * @param name key de la enumeración
     */
    public Role(RoleName name) {
        this.name = name;
    }

}
