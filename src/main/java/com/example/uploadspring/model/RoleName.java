package com.example.uploadspring.model;

/**
 * Enumeración definida para indicar los roles de los usuarios
 */
public enum  RoleName {
    ROLE_USER,
    ROLE_ADMIN
}
