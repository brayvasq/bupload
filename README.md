### Spring Boot Uploads

#### Obteniendo información de .properties

```yaml
#application.properties
file.upload-dir=./uploads
```

Se debe crear una clase que posea la anotación `@ConfigurationProperties`

```java
// FileStorageProperties.java
@ConfigurationProperties(prefix = "file")
@Data
public class FileStorageProperties {
    private String uploadDir;
}
```

**Nota:** La anotación `@Data` es de el plugin para java `Lombok`, esta anotación permite crear clases omitiendo el constructor, getters, setters, toString, etc.

En caso de que haya error con la anotación `@ConfigurationProperties` se debe añadir la dependencia al archivo `poml.xml`

```xml
<!-- poml.xml -->
...
<dependency>
	<groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-configuration-processor</artifactId>
    <optional>true</optional>
</dependency>
...
```

#### Ejecución

```bash
mvn spring-boot:run
```

#### Previews

![login](https://github.com/brayvasq/upload-spring/blob/master/previews/login_upload.PNG)

![list](https://github.com/brayvasq/upload-spring/blob/master/previews/list_uploads.PNG)

#### Referencias

- https://www.callicoder.com/spring-boot-file-upload-download-jpa-hibernate-mysql-database-example/
- https://www.callicoder.com/spring-boot-file-upload-download-rest-api-example/
- https://www.callicoder.com/spring-boot-spring-security-jwt-mysql-react-app-part-2/
